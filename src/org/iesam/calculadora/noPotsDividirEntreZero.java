package org.iesam.calculadora;


public class noPotsDividirEntreZero extends Exception {

	public noPotsDividirEntreZero() {
		super();
	}

	public noPotsDividirEntreZero(String n) {
		super(n);
	}

}