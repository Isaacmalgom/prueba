package org.iesam.calculadora;

import static org.junit.Assert.*;

public class Test {

	@org.junit.Test
	public void sumar() {
		Calculadora suma = new Calculadora();
		
		assertEquals(suma.sumar(1, 2), 3);
		
	}
	 @org.junit.Test
	 public void restar() {
		 Calculadora resta = new Calculadora();
		 
		 assertEquals(resta.restar(2, 1),1);
	 }

	 @org.junit.Test
	 public void multipilcar() {
		 Calculadora multiplicar = new Calculadora();
		 // el assertEquals con longs es diferente que el double , el de long primero ponemos el resultado esperado
		 // llamamos al metodo y luego ponemos el rango de error que puede generar.
		 assertEquals(3, multiplicar.multiplicar(2, 1),1);
		 
		 
	 }
	 
	 // tenemos que poner en el test que espera una excepcion as� si lanza la excepcion el test nos
	 //lo dar� por  bueno pene
	 @org.junit.Test(expected = noPotsDividirEntreZero.class)
	 public void dividir() throws noPotsDividirEntreZero {
		 Calculadora calc = new Calculadora();
		 
		 assertEquals(5, calc.dividir(2, 0),0);
		 
		 
	 }
}
